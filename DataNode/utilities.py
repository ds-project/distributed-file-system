from time import sleep
from threading import Thread
import requests


def replicate_to_peers(peer_list, PORT, name):
    [Thread(target=send_chunk, args=(s, PORT, name)).start() for s in peer_list]


def send_chunk(peer, PORT, name):
    # send chunk to peer
    chunk = {'chunk': open(name, 'rb')}
    r = requests.post(f'http://{peer}:{PORT}/api/upload_replica', files=chunk, data={"name": name})
    print(f'    # sent chunk {name} to peer {peer}:{PORT}')


def replicate_from_peer(peer, PORT, self_ip):
    def tak_nelza(peer, PORT, self_ip):
        sleep(3)
        # send request for replication -> peer will upload its chunks
        r = requests.post(f'http://{peer}:{PORT}/api/replicate', data={"ip_addr": self_ip})

    Thread(target=tak_nelza, args=(peer, PORT, self_ip)).start()
    return 'good'


def replicate_to_peer(peer, PORT, chunk_list):
    [Thread(target=send_chunk, args=(peer, PORT, c)).start() for c in chunk_list]
