from hashlib import md5
import requests
import socket
import shutil
import json
import os

from flask import Flask, request, jsonify, Blueprint, Response, send_from_directory
from flask_cors import CORS
from shutil import copyfile
from utilities import replicate_to_peers, replicate_from_peer, replicate_to_peer

app = Flask(__name__)
CORS(app)
app_blueprint = Blueprint("API", __name__)

SELF_ADR = ''
PEER = ''
NAME_NODE_ADR = os.environ.get('NAME_NODE_ADR') if 'NAME_NODE_ADR' in os.environ else '0.0.0.0'
NAME_NODE_PORT = os.environ.get('NAME_NODE_PORT') if 'NAME_NODE_PORT' in os.environ else 5001

PORT = os.environ.get('PORT') if 'PORT' in os.environ else 5006
DATA_ROOT = './data'


@app_blueprint.route('/healthcheck')
def healthcheck():
    host_name = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)

    return f"working ip: {host_ip} name: {host_name}"


@app_blueprint.route('/upload_replica', methods=['POST'])
def upload_replica():
    chunk = request.files.get('chunk')
    name = request.form.get('name')

    chunk.save(name)

    return Response(status=200)


@app_blueprint.route('/upload_chunk', methods=['POST'])
def upload_chunk():
    chunk = request.files.get('chunk')
    chunk_id = request.form.get('chunk_id')

    # check params consistency
    if not (chunk and chunk_id):
        return Response(status=400,
                        response=f'parameters were not provided: '
                                 f'{"chunk" if chunk_id else "chunk_id"}')

    data = {}
    #  end ack to name node
    r = requests.post(f'http://{NAME_NODE_ADR}:{NAME_NODE_PORT}/api/acknowledge', data={'chunk_id': chunk_id})

    if r.status_code == 200:
        chunk.seek(0)
        fname = f'{DATA_ROOT}/id_{chunk_id}_fn_{chunk.filename}'
        chunk.save(fname)

        peer_servers = [s['ip_address'] for s in r.json()['servers']]
        peer_servers.remove(SELF_ADR)

        # start replication
        replicate_to_peers(peer_servers, PORT, fname)

        data['msg'] = 'chunk successfully acknowledged, replication started'
    else:
        data['msg'] = r.text

    return Response(status=r.status_code, response=json.dumps(data))


def find(name, path):
    for root, dirs, files in os.walk(path):
        for file in files:
            if name in file:
                return root, file
    return None


@app_blueprint.route('/download_chunk/<chunk_id>', methods=['GET'])
def download_chunk(chunk_id):
    dir, fname = find(f'id_{chunk_id}', f'{DATA_ROOT}/')

    if not fname:
        return Response(status=404, response=fname)

    return send_from_directory(dir, fname, as_attachment=True)


@app_blueprint.route('/replicate', methods=['POST'])
def replicate():
    peer = request.form.get('ip_addr')

    # list of all files in data folder
    chunk_list = []
    for root, dirs, files in os.walk(DATA_ROOT):
        for file in files:
            chunk_list.append(os.path.join(root, file))
    replicate_to_peer(peer, PORT, chunk_list)
    return Response(status=200)


# system reset (delete all files in /data folder)
@app_blueprint.route('/reset', methods=['POST'])
def reset():
    shutil.rmtree(DATA_ROOT, ignore_errors=True)
    os.mkdir(DATA_ROOT)
    return Response(status=200)


# system reset (delete all files in /data folder)
@app_blueprint.route('/copy', methods=['POST'])
def copy():
    original = request.form.get('original')
    copy = request.form.get('copy')

    dir, fname = find(f'id_{original}', f'{DATA_ROOT}/')
    new_fname = fname.split('_')
    new_fname = f'{new_fname[0]}_{copy}_{new_fname[2]}_copy_{new_fname[3]}'

    copyfile(os.path.join(dir, fname), os.path.join(dir, new_fname))

    return Response(status=200)


app.register_blueprint(app_blueprint, url_prefix="/api")
if __name__ == '__main__':

    # register machine on start
    try:
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        SELF_ADR = host_ip
        print('    # sending request for registration in namenode')
        r = requests.post(f'http://{NAME_NODE_ADR}:{NAME_NODE_PORT}/api/register_dn', data={'ip_address': host_ip})
        print(f'    # datanode {r.json()["msg"]} \n    # starting server [{SELF_ADR}] on port [{PORT}]')

        # pull replicas
        if 'peer' in r.json():
            print(f'    # starting pull replicas from {r.json()["peer"]}:{PORT}')
            status = replicate_from_peer(r.json()['peer'], PORT, SELF_ADR)
            print(f'    # {status}')

        app.run(host='0.0.0.0', port=PORT)

    except requests.exceptions.ConnectionError as e:

        print(f'unable to start datanode \nan error has occur: \n{e}')
        exit()
