from threading import Thread
import requests
import os
import time


def reset_peers(peer_list, DATA_NODE_PORT):
    [Thread(target=reset_peer, args=(s, DATA_NODE_PORT)).start() for s in peer_list]


def reset_peer(peer, DATA_NODE_PORT):
    print(f'reseting peers {peer}:{DATA_NODE_PORT}')
    r = requests.post(f'http://{peer}:{DATA_NODE_PORT}/api/reset')


def copy_chunks(server_list, copy_chunk_list, DATA_NODE_PORT):
    [Thread(target=copy_chunks_on_peer, args=(s, copy_chunk_list, DATA_NODE_PORT)).start() for s in server_list]


def copy_chunks_on_peer(server, copy_chunk_list, DATA_NODE_PORT):
    for copy_chunk in copy_chunk_list:
        original = copy_chunk[0]
        copy = copy_chunk[1]
        r = requests.post(f'http://{server}:{DATA_NODE_PORT}/api/copy', data={"original": original, "copy": copy})


def availability_check(Server, db):
    def check(Server, db):
        while True:
            servers = Server.query.filter(Server.status == True).all()
            for server in servers:
                response = os.system(f"ping -c 1 {server.ip_address}")
                if response != 0:
                    server.status = False
                    db.session.add(server)
                    db.session.commit()
            time.sleep(0.5)

    Thread(target=check, args=(Server, db)).start()
