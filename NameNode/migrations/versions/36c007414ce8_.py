"""empty message

Revision ID: 36c007414ce8
Revises: 495d508a7044
Create Date: 2019-11-11 20:36:32.598155

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '36c007414ce8'
down_revision = '495d508a7044'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('items_parent_id_fkey', 'items', type_='foreignkey')
    op.create_foreign_key(None, 'items', 'items', ['parent_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'items', type_='foreignkey')
    op.create_foreign_key('items_parent_id_fkey', 'items', 'items', ['parent_id'], ['id'])
    # ### end Alembic commands ###
