from math import ceil
import os
import json
import socket

from flask import Flask, request, jsonify, Blueprint, Response
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import sqlalchemy
from config import DevelopmentConfig
from utilities import reset_peers, copy_chunks, availability_check

app = Flask(__name__)
CORS(app)
app_blueprint = Blueprint("API", __name__)

app.config.from_object(DevelopmentConfig)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

CHUNK_SIZE = 5 * 1024 * 1024
REPLICATION = 3
DATA_NODE_PORT = os.environ.get('DATA_NODE_PORT') if 'DATA_NODE_PORT' in os.environ else 5006

from models import Item, Chunk, Server

# availability_check(Server, db)

AVL_SPACE = 1000 * 1024 * 1024


@app_blueprint.route('/healthcheck')
def healthcheck():
    host_name = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)

    return f"working ip: {host_ip} name: {host_name}"


@app_blueprint.route("/list_directory", methods=['GET'])
def list_root():
    try:
        items = Item.query.filter(Item.parent_id == None).all()

        return jsonify([e.serialize_less() for e in sorted(items, key=lambda x: x.created_date, reverse=True)])

    except Exception as e:
        return str(e)


@app_blueprint.route("/list_directory/<dir_id>", methods=['GET'])
def list_directory(dir_id):
    """
    method returns all items inside directory
    :param dir_id: id of the directory
    :return: list of all items in it
    """
    try:
        item = Item.query.get(dir_id)
        if not item:
            return Response(status=404, response='Object is not found')
        if item.is_dir:

            return jsonify(
                [e.serialize_less() for e in sorted(item.children, key=lambda x: x.created_date, reverse=True)])
        else:
            return Response(status=400, response='Object is not a directory')
    except Exception as e:
        return str(e)


@app_blueprint.route("/mkdir", methods=['PUT'])
def mkdir():
    data = request.json
    name = data['name']
    directory_id = data['directory_id'] if 'directory_id' in data.keys() else None

    if not name:
        return Response(status=400, response=f'Parameters were not provided: name')

    if Item.query.filter(Item.name == name).filter(Item.parent_id == directory_id).all():
        return Response(status=400, response=f'Directory with name: {name} already exist in this directory')

    try:
        item = Item(name=name, is_dir=True, parent_id=directory_id)
        db.session.add(item)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return Response(status=400, response=str(e))

    return Response(status=201, response=json.dumps(item.serialize()))


@app_blueprint.route("/rm/<id>", methods=['DELETE'])
def rmdir(id):
    directory_id = id
    item = Item.query.get(directory_id)

    db.session.delete(item)
    db.session.commit()
    return Response(status=200, response=f'Directory {item.name} deleted!')


@app_blueprint.route("/move", methods=['PATCH'])
def move():
    data = request.json
    directory_id = data['directory_id'] if 'directory_id' in data.keys() else None
    file_id = data['file_id']

    item = Item.query.get(file_id)
    if Item.query.filter(Item.name == item.name).filter(Item.parent_id == directory_id).all():
        return Response(status=400, response=f'Item with name: {item.name} already exist in this directory')

    item.parent_id = directory_id

    db.session.commit()
    return Response(status=200, response=f'item {item.name} moved!')


@app_blueprint.route("/copy", methods=['PATCH'])
def copy():
    data = request.json
    file_id = data['file_id']

    item = Item.query.get(file_id)

    new_item = Item(name=f'copy_{item.name}', is_dir=item.is_dir, parent_id=item.parent_id, size=item.size)
    db.session.add(new_item)

    # todo copy chunks

    copy_chunk_list = []
    for chunk in item.chunks:
        new_chunk = Chunk(offset=chunk.offset, size=chunk.size, file_id=new_item.id)
        db.session.add(new_chunk)
        db.session.commit()
        copy_chunk_list.append((chunk.id, new_chunk.id))

    server_list = [s.ip_address for s in Server.query.filter(Server.status == True).all()]

    copy_chunks(server_list, copy_chunk_list, DATA_NODE_PORT)

    db.session.commit()
    return Response(status=200, response=json.dumps(new_item.serialize()))


@app_blueprint.route("/reset", methods=['DELETE'])
def reset():
    db.session.query(Item).delete()
    db.session.commit()
    peer_list = [s.ip_address for s in Server.query.filter(Server.status == True).all()]
    # send request for reset on datanodes
    reset_peers(peer_list, DATA_NODE_PORT)
    AVL_SPACE = 1000 * 1024 * 1024
    data = {"msg": "dfs reseted!", "space": AVL_SPACE}
    return Response(status=200, response=json.dumps(data))


@app_blueprint.route("/upload_file", methods=['POST'])
def upload_file():
    data = request.json

    file_name = data['file_name']
    size = data['size']
    directory_id = data['directory_id'] if 'directory_id' in data.keys() else None

    # check params consistency
    if not (file_name and (size != None)):
        return Response(status=400, response=f'Parameters were not provided: '
                                             f'{"size" if file_name else "file_name"}')

    # check uniqueness of item in directory
    if Item.query.filter(Item.name == file_name).filter(Item.parent_id == directory_id).all():
        return Response(status=400, response=f'File with file_name: {file_name} already exist in this directory')

    # register new file
    try:
        item = Item(name=file_name, size=size, parent_id=directory_id)
        db.session.add(item)
        db.session.commit()
    except sqlalchemy.exc.IntegrityError as e:
        db.session.rollback()
        return Response(status=404, response=f'No such directory with id {directory_id}')

    number_of_chunks = ceil(int(item.size) / CHUNK_SIZE)

    # # get set of available servers
    # servers = Server.query.filter(Server.status == True).all()
    # if not servers:
    #     return Response(status=404, response='no servers avvailable')
    #
    # servers = cycle(sorted(servers, key=lambda x: x.memory_occupied))

    # create set of chunks and assign server for chunk
    for i in range(number_of_chunks):
        offset = i * CHUNK_SIZE
        size = min(CHUNK_SIZE, int(item.size) - offset)
        # server = next(servers)
        chunk = Chunk(offset=offset, size=size, file_id=item.id)
        db.session.add(chunk)

    db.session.commit()

    data = item.serialize()
    data['size_of_chunks'] = CHUNK_SIZE
    return Response(status=201, response=json.dumps(data))


@app_blueprint.route("/register_dn", methods=['POST'])
def register_dn():
    ip_addr = request.form.get('ip_address')
    if Server.query.filter(Server.ip_address == ip_addr).all():
        return Response(status=400, response=json.dumps({"msg": f'already registered'}))

    # create new server instance
    server = Server(ip_address=ip_addr, status=True)

    db.session.add(server)
    db.session.commit()
    # send request for replication for the chunks on newly created server
    server = Server.query.filter(Server.status == True).filter(Server.ip_address != ip_addr).all()
    if server:
        return Response(status=200, response=json.dumps({"msg": 'registered successfully, pull data from peer',
                                                         "peer": server[0].ip_address}))
    return Response(status=200, response=json.dumps({"msg": 'registered successfully'}))


@app_blueprint.route("/download_file/<file_id>", methods=['GET'])
def download_file(file_id):
    item = Item.query.get(file_id)

    return Response(status=200, response=json.dumps(item.serialize()))


@app_blueprint.route("/acknowledge", methods=['POST'])
def acknowledge():
    chunk_id = request.form.get('chunk_id')
    chunk = Chunk.query.get(chunk_id)

    if not chunk:
        return Response(status=404, response=f'There is no chunk with id {chunk_id}')

    if chunk.acknowledged:
        return Response(status=400, response='Chunk already acknowledged')

    chunk.acknowledged = True
    db.session.add(chunk)
    db.session.commit()

    data = dict()
    data['message'] = 'Chunk successfully acknowledged, start replication'
    # replication server pull
    data['servers'] = [s.serialize() for s in Server.query.filter(Server.status == True).all()]

    return Response(status=200, response=json.dumps(data))


app.register_blueprint(app_blueprint, url_prefix="/api")
if __name__ == '__main__':
    app.run()
