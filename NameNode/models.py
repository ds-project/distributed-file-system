from app import db
from sqlalchemy.orm import backref
import datetime


class Item(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    is_dir = db.Column(db.Boolean, default=False)
    children_self_ref = db.relationship('Item',
                                        remote_side='Item.id',
                                        backref=backref("children", cascade="delete"),
                                        uselist=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('items.id'), nullable=True)
    size = db.Column(db.Integer)
    created_date = db.Column(db.DateTime, default=datetime.datetime.now)
    chunks = db.relationship('Chunk', backref='items', lazy=True, cascade='delete')

    def __repr__(self):
        return f'<id: {self.id} name: {self.name}>'

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'is_dir': self.is_dir,
            'size': len(self.children) if self.is_dir else self.size,
            'parent_id': self.parent_id,
            'children': [child.serialize_less() for child in self.children],
            'chunks': [chunk.serialize() for chunk in self.chunks],
            'number_of_chunks': len(self.chunks),
            'created_date': str(self.created_date)
        }

    def serialize_less(self):
        return {
            'id': self.id,
            'name': self.name,
            'is_dir': self.is_dir,
            'size': len(self.children) if self.is_dir else self.size,
            'parent_id': self.parent_id,
            'number_of_chunks': len(self.chunks),
            'created_date': self.created_date
        }


class Server(db.Model):
    __tablename__ = 'servers'

    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String())
    # todo update on upload ack
    memory_occupied = db.Column(db.Integer, default=0)
    # todo check status if new node (might be not have all files instantly)
    status = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'<id: {self.id} ip_address: {self.ip_address} memory_occupied: {self.memory_occupied}>'

    def serialize(self):
        return {
            'id': self.id,
            'ip_address': self.ip_address,
            'status': self.status
        }


servers = db.Table('servers_link',
                   db.Column('server_id', db.Integer, db.ForeignKey('servers.id', ondelete="CASCADE"),
                             primary_key=True),
                   db.Column('chunk_id', db.Integer, db.ForeignKey('chunks.id', ondelete="CASCADE"), primary_key=True)
                   )


class Chunk(db.Model):
    __tablename__ = 'chunks'

    id = db.Column(db.Integer, primary_key=True)
    offset = db.Column(db.Integer)
    size = db.Column(db.Integer)
    file_id = db.Column(db.Integer, db.ForeignKey('items.id', ondelete='CASCADE'), nullable=False)
    acknowledged = db.Column(db.Boolean, default=False)
    servers = db.relationship('Server', secondary=servers, lazy='subquery',
                              backref=db.backref('chunks', lazy=True, cascade='delete'))

    def __repr__(self):
        return f'<file_id: {self.file_id} offset: {self.offset} size: {self.size}>'

    def serialize(self):
        return {
            'id': self.id,
            'offset': self.offset,
            'size': self.size,
            'file_id': self.file_id
        }
