## Group Project Assignment: Distributed File System
Team: Pavel Nikulin (DS1), Aygul Malikova (SE1), Slava Goreev (SB)

## Getting Started

This repository contains implementation of DFS backend part

[The DFS API documentation](https://hackmd.io/WwfjAICGQaS6zGAqxcOGQw?both)

### Prerequisites

What things you need to install

```
docker
docker-compose
docker swarm 
```

## Description of communication protocols

NameNode and DataNodes use HTTP to communicate with each other

## Architectural diagrams

![](https://sun9-14.userapi.com/c855416/v855416036/16bfaf/FuAlk0wMLWs.jpg)

## Deployment

To deploy dfs you need to do following:

1. set up ```docker swarm``` claster
2. run ```docker stack deploy -c docker-compose.yml dfs```
3. check status ```docker service ls```


## Built With

* [Flask](https://www.palletsprojects.com/p/flask/) - The server framework used
* [PostgreSQL](https://www.postgresql.org) - Database Management
* [Docker](https://www.docker.com) - Packing and deployment
